var canvas = new fabric.Canvas('canvas');

function rectangle(top, left, width, height, fillcolor, strokewidth, strokecolor){
	var rect = new fabric.Rect({
	    top : top,
	    left : left,
	    width : width,
	    height : height,
	    fill : fillcolor,
	    strokewidth: strokewidth,
	    stroke: strokecolor
  });

  canvas.add(rect);
}

function ellipse(top, left, xradius, yradius, fillcolor, strokewidth, strokecolor){
	var ell = new fabric.Ellipse({
		  left: left,
		  top: top,
		  rx: xradius,
		  ry: yradius,
		  fill: fillcolor,
		  stroke: strokecolor,
		  strokeWidth: strokewidth
	});

	canvas.add(ell);
}

function drawRect(){

var x,y,w,h;	

canvas.on('mouse:down', function(e) {
  x = e.e.offsetX;
  y = e.e.offsetY;
});

canvas.on('mouse:up', function(e) {
  w = e.e.offsetX - x;
  h = e.e.offsetY - y;

  rectangle(y, x, w, h, 'white', 5, 'black');

});

canvas.on('object:moving', function(e) {
  var activeObject = e.target;
  console.log(activeObject.get('left'), activeObject.get('top'));
});

canvas.on('object:scaling', function(e) {
  var activeObject = e.target;
  console.log(activeObject.get('left'), activeObject.get('top'));
});

}

function drawEllipse(){

var x,y,w,h;	

canvas.on('mouse:down', function(e) {
  x = e.e.offsetX;
  y = e.e.offsetY;
});

canvas.on('mouse:up', function(e) {
  w = e.e.offsetX - x;
  h = e.e.offsetY - y;

  ellipse(y, x, w, h, 'white', 2, 'black');

});

}
